#include "request.h"
#include "server_thread.h"
#include "common.h"




struct data{
    
    
    char* key;
    
    struct file_data my_data;
   
   // struct data* left;

    struct data* right;
    
    struct data* next;
    
   // unsigned int counter ;
    
    
};



struct LRU {
    
    
    struct data* head;
    
    
    
};


struct hashtable {
    
    unsigned int size;
    
    unsigned int max_cache_size;
    
    unsigned int current_cache_size;
    
    struct data** my_data;
    
    pthread_mutex_t my_lock;
    
};




struct hashtable my_cache;

struct LRU my_lru;



void cache_int (int max_cache_size){
    

   // double load_factor = 0.75;
    
   unsigned int hashtable_size  = 10000;
    
   my_cache.max_cache_size = max_cache_size ;
   
   my_cache.current_cache_size = 0;
           
    my_cache.size  =  hashtable_size ;
    
   my_cache.my_data = malloc((sizeof(struct data*) + 1) * (hashtable_size));
  
   unsigned i = 0 ; 
   
    for(i = 0; i <hashtable_size ;i++){
        
        (my_cache.my_data)[i] =NULL;
        
    }
    
    return ;
    
}


unsigned int stringToHash(char *word, unsigned int hashTableSize){
    unsigned int counter, hashAddress =0;
    for (counter =0; word[counter]!='\0'; counter++){
        hashAddress = hashAddress*word[counter] + word[counter] + counter;
    }
    return (hashAddress%hashTableSize);
}







struct data * pop_from_LRU(char* name){
   
    
    
    struct data* temp =my_lru.head;
    
    if (temp->key == name)
        
    {
        
        my_lru.head =my_lru.head->next;
        
        temp->next = NULL;
       
        return temp;
        
        
    }
    
    struct data* temp1 = my_lru.head;
    
    struct data* temp2 = temp1->next;
    
    while (temp2 != NULL) {
        
        if (temp2->key == name) {
            
            temp1->next = temp2->next;
            
            temp2->next =NULL;
            
           // my_lru.size --;
            return temp2 ;
        }
        temp1= temp1->next;
        temp2 = temp2->next;
        
    }
    
    
    return NULL;
    
    
}







int add_new_data(struct data* new_data){
    
    struct data * temp  = my_lru.head ;
    
    
    
    //printf("size:%d\n",myque->size);
    if (temp == NULL) {
        
        my_lru.head = new_data;
        // new_thread ->prev =  NULL;
        new_data ->next = NULL;
        return 1;
    }
    
    while(temp->next!= NULL){
        
        temp = temp ->next ;
        
    }
    
    temp ->next = new_data ;
    
    new_data ->next = NULL ;
    
    return 1;
    
    
}








struct data * pop_from_my_cache(char* name){
    
    
    unsigned int ID = stringToHash(name, my_cache.size);
    
    struct data* temp =my_cache.my_data[ID];
    
    if (temp->key == name)
        
    {
        
        my_cache.my_data[ID] =my_cache.my_data[ID]->right;
        
        temp->right = NULL;
        
        
        my_cache.size = my_cache.size - temp->my_data.file_size;
        return temp;
        
        
    }
    
    struct data* temp1 = my_cache.my_data[ID];
    
    struct data* temp2 = temp1->right;
    
    while (temp2 != NULL) {
        
        if (temp2->key == name) {
            
            temp1->right = temp2->right;
            
            temp2->right =NULL;
            
            my_cache.current_cache_size = my_cache.current_cache_size - temp2->my_data.file_size;
            
            return temp2 ;
        }
        temp1= temp1->right;
        temp2 = temp2->right;
        
    }
    
    
    return NULL;
    
    
}


struct data* cache_lookup(struct file_data *file){
    
   
    
    
    unsigned int index = stringToHash(file->file_name, my_cache.size) ;
    
    struct data* temp = my_cache.my_data[index] ;
    
    
    if (temp == NULL) {
        
       // pthread_mutex_unlock(&(my_cache.my_lock));
       
        return NULL;
    }
    
  
    if (strcmp(temp->key,file->file_name) ==0) {
       
       // pop_from_LRU(file->file_name);
        
       // add_new_data(temp);
        
       // pthread_mutex_unlock(&(my_cache.my_lock));
        
        return temp ;
    }
    
    while (temp->right != NULL ) {
        
        temp= temp->right ;
        
        if (strcmp(temp->key,file->file_name) ==0) {
            
            
           // pop_from_LRU(file->file_name);
            
           // add_new_data(temp);
          
            
           
            return temp ;
        }
        
        
     
        
    }
    
    
    
   // pthread_mutex_unlock(&(my_cache.my_lock));
    return NULL;
    
}






void cache_evict(struct file_data * file){
    

    
   struct data * temp =  my_lru.head ;
    
    pop_from_LRU(temp->key);
    
    pop_from_my_cache(temp->key);

    
    return ;
    
    
    
}




int cache_insert(struct file_data * file , unsigned max_cache_size){
    
    
    //pthread_mutex_lock(&(my_cache.my_lock)) ;
    
    //while(my_cache.current_cache_size  + file->file_size >  max_cache_size) {
        
    //    cache_evict(file);
        
    //}
    
    
     unsigned ID =  stringToHash(file->file_name , my_cache.size);
    
    struct data** node = &my_cache.my_data[ID];
    
        struct data* new_item = malloc(sizeof(struct data) +1) ;
        
        //new_item->key = malloc(sizeof(strlen(file->file_name)+1));
        
        
        
        new_item ->key = file->file_name ;
        
        
        
        new_item ->right =NULL;
        
        new_item->my_data.file_name = new_item->key;
        
        
        new_item->my_data.file_size = file->file_size;
        
        //new_item->my_data.file_buf = malloc(sizeof(file->file_buf));
        
        new_item->my_data.file_buf = file->file_buf;
        
    
        
        my_cache.current_cache_size  = my_cache.current_cache_size  + file->file_size ;
        
        //add_new_data(new_item);
        
    
    if (*node == NULL) {
        
       my_cache.my_data[ID] = new_item ;
    }
    
    else{
        
        while ((*node)->right != NULL) {
            
            *node = (*node)->right;
        }
        
        (*node)->right = new_item ;
        
        
    }
    
    
    
    
    
       // pthread_mutex_unlock(&(my_cache.my_lock));
        return  0;

    
    

}



struct server {

	
	pthread_mutex_t my_mutex;
	
	int* buffer;	
	
	int in ;

	int out; 

	int nr_threads;
	int max_requests;
	int max_cache_size;
	/* add any other parameters you need */
};

pthread_cond_t  Full;

pthread_cond_t  Empty;




/* static functions */

/* initialize file data */
static struct file_data *
file_data_init(void)
{
	struct file_data *data;

	data = Malloc(sizeof(struct file_data));
	data->file_name = NULL;
	data->file_buf = NULL;
	data->file_size = 0;
	return data;
}

/* free all file data */
static void
file_data_free(struct file_data *data)
{
	//free(data->file_name);
       // if(data->file_buf != NULL)
	//free(data->file_buf);
	free(data);
}

static void
do_server_request(struct server *sv, int connfd)
{
	int ret;
	struct request *rq;
	struct file_data *data;

	data = file_data_init();

	/* fills data->file_name with name of the file being requested */
	rq = request_init(connfd, data);
	if (!rq) {
		file_data_free(data);
		return;
	}
	/* reads file, 
	 * fills data->file_buf with the file contents,
	 * data->file_size with file size. */
        pthread_mutex_lock(&(my_cache.my_lock)) ; 
       struct data* check = cache_lookup(data) ;
        pthread_mutex_unlock(&(my_cache.my_lock)) ;
        
     // int flag = 0;
       if(check!= NULL){
           
          //request_set_data(rq,&(check->my_data)) ;
           data->file_buf = check->my_data.file_buf;
          data->file_size = check->my_data.file_size;
          ret =1;
       }
        
       else{
	
       ret = request_readfile(rq);
        pthread_mutex_lock(&(my_cache.my_lock)) ;
        
        if(data->file_size < my_cache.max_cache_size){
       cache_insert(data, my_cache.max_cache_size);}
        
       pthread_mutex_unlock(&(my_cache.my_lock)) ;
     //flag =1;
       
       }
        if (!ret)
		goto out;
	/* sends file to client */
	request_sendfile(rq);
out:
	request_destroy(rq);
        
       //if(flag ==0){
	//file_data_free(data);
       //}
}




void consumer( void * ab){
	
	struct server *sv = (struct server *) ab ;

	while(1){
	
	pthread_mutex_lock(&(sv->my_mutex));
	
        
        while(sv->in ==sv->out){
         
            
           pthread_cond_wait(&Empty, &(sv->my_mutex)); 
            
            
        }
        
        int data = sv->buffer[sv->out];
        
         if((sv->in - sv->out + (sv->max_requests+1)) % (sv->max_requests+1) == sv->max_requests ) {
		  
		pthread_cond_signal(&Full);

            }
        sv->out = (sv->out+1)% (sv->max_requests+1) ;

        pthread_mutex_unlock(&(sv->my_mutex));
        do_server_request(sv, data);
	}

       
        

}



/* entry point functions */



struct server *
server_init(int nr_threads, int max_requests, int max_cache_size)
{
	struct server *sv;

	sv = malloc(sizeof(struct server));
	sv->nr_threads = nr_threads;
	sv->max_requests = max_requests;
	sv->max_cache_size = max_cache_size;

	if (nr_threads > 0 || max_requests > 0 || max_cache_size > 0) {
		
		pthread_mutex_init(&(sv->my_mutex), NULL) ;
		
		 pthread_cond_init(&Full, NULL);	
		
		 pthread_cond_init(&Empty, NULL);

		sv->buffer  = malloc(sizeof(int)*(max_requests+1));

		sv->in = 0;

		sv->out = 0;
	
		pthread_t t_array[nr_threads];


		int i = 0;
		
		for( i =0;i <nr_threads; i++){
	
	
		 pthread_create( &t_array[i],NULL, (void *)consumer,(void *)sv);

		}
	
	}


	/* Lab 4: create queue of max_request size when max_requests > 0 */

	/* Lab 5: init server cache and limit its size to max_cache_size */
         cache_int(max_cache_size); 
	/* Lab 4: create worker threads when nr_threads > 0 */

	return sv;
}

void
server_request(struct server *sv, int connfd)
{
	if (sv->nr_threads == 0) { /* no worker threads */
		do_server_request(sv, connfd);
	} else {
		pthread_mutex_lock(& (sv->my_mutex));
	

            while ((sv->in - sv->out + (sv->max_requests+1)) % (sv->max_requests+1) == sv->max_requests ) {
		  
		pthread_cond_wait(&Full, &(sv->my_mutex));

            }

          sv->buffer[sv->in] = connfd ;
          
          if(sv->in == sv->out){
              
              pthread_cond_signal(&Empty);      
          
          }
          
          sv->in = (sv->in +1)% (sv->max_requests +1); 
          
          pthread_mutex_unlock(&(sv->my_mutex));
          
            }
        
        
}
