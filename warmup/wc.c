#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include "common.h"
#include "wc.h"
#include <string.h>
#include <ctype.h>
struct word{
 	char *key;
	int count;
	struct word * left ;
    struct word * right;

};


struct wc {
	struct word** hashtable;
	unsigned int size ;
/* you can define this struct to have whatever fields you want. */
};



unsigned int stringToHash(char *word, unsigned int hashTableSize){
    unsigned int counter, hashAddress =0;
    for (counter =0; word[counter]!='\0'; counter++){
        hashAddress = hashAddress*word[counter] + word[counter] + counter;
    }
    return (hashAddress%hashTableSize);
}



int insert(struct word** node , char* wordss){
    
    
    if((*node) == NULL){
        struct word* new_item = malloc(sizeof(struct word) +1) ;
        new_item ->count  = 1;
        new_item->key = malloc(sizeof(char)*(strlen(wordss)+1));
        strcpy(new_item ->key ,wordss) ;
        new_item ->left = NULL;
        new_item ->right =NULL;
        *node = new_item;
        return  0;
    }
    
    
    else {
        
        int ret_value = strcmp(wordss,(*node)->key) ;
        if (ret_value < 0) {
           return insert(&((*node)->left), wordss);
            
        }
        if(ret_value > 0){
            return insert(&((*node)->right), wordss);
            
        }
     
    
    
    }
    
    (*node)->count  = (*node)->count + 1;
    return 1;
    
}



struct wc *
wc_init(char *word_array, long size)
{
	struct wc *wc;
	char s[]= {" \t\n\r"};
	wc = (struct wc *)malloc(sizeof(struct wc));
	assert(wc);
	double load_factor = 0.75;
	
	unsigned int hashtable_size  = (int)size/(load_factor*5) ;

	wc->size  =  hashtable_size ;
	
	wc-> hashtable = malloc((sizeof(struct word*) + 1) * (wc->size));
	
	

	
	int i =0;
	

	

	for(i = 0; i <hashtable_size ;i++){
			(wc->hashtable)[i] =NULL;
	
    }
    
    
	char *array_copy = malloc(sizeof(char)*(1+strlen(word_array)));	
	strcpy(array_copy,word_array);
	

	
	char*  wordss = strtok(array_copy,s);
	
	
    while(wordss != NULL){
        

		if(strcmp("\n",wordss)!=0){
        unsigned int index  = stringToHash(wordss, hashtable_size);
        
         insert(&((wc->hashtable)[index]), wordss);}
        
         wordss = strtok(NULL , s);
      
       
        
        
    }

    free(array_copy);


	

	return wc;
}



void print_all_item(struct word* node) {
    if (node == NULL) {
        return;
    }
    printf("%s:%d\n" , node->key,node->count);
    print_all_item(node->left);
    print_all_item(node->right);
    
    }


void
wc_output(struct wc *wc)
{
	int i = 0;
    for ( i =0; i< wc->size; i++) {
        print_all_item((wc->hashtable) [i]);
    }
    
    return;
    
    
    
}




void destory_tree(struct word ** node) {
    
    struct word * current  = *node;
    
    if(current == NULL){
        return ;
    }
    
    if (current ->left != NULL) {
        destory_tree(&(current->left));
        
    }
    if (current->right != NULL) {
        destory_tree(&(current->right));
    }
    free((*node) ->key);
    free(*node);
    *node = NULL;
    return;
    
    
    
}






void
wc_destroy(struct wc *wc)
{

	int i = 0;
    for (i =0 ; i< wc->size; i++) {
        destory_tree(&(wc->hashtable[i]));
        
    }
    
    
    
    free(wc->hashtable);
	free(wc);
}
