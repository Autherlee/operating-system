#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include "common.h"
#include "point.h"
#include "sorted_points.h"



struct sorted_points {
    struct points *head;
    
    /* you can define this struct to have whatever fields you want. */
};

struct points{
    double x;
    double y;
    double distance;
    struct points* next;
};

struct sorted_points *
sp_init()
{
    struct sorted_points *sp;
    
    sp = (struct sorted_points *)malloc(sizeof(struct sorted_points)+1);
    sp->head = NULL;
    assert(sp);
    return sp;
}

void
sp_destroy(struct sorted_points *sp)
{
    struct points* temp = sp->head;
    struct points* temp1 = sp->head;
    if(sp->head ==NULL){
        free(sp);
        sp = NULL;
        return;
    }
    while(temp->next != NULL){
        temp1 = temp->next;
        free(temp);
        temp = temp1;
    }
    free(temp);
    free(sp);
}

int
sp_add_point(struct sorted_points *sp, double x, double y)
{
    struct points *new_point = malloc(sizeof(struct points)+1);
    new_point->next = NULL;
    new_point->x = x;
    new_point->y = y;
    new_point->distance = x*x + (y*y);
    if(sp->head==NULL){
        struct points* temp_1 = sp->head;
        sp->head =  new_point;
        new_point->next = temp_1;
        return 1;
    }
    if(sp->head->distance > (new_point->distance) ){
        struct points* temp_1 = sp->head;
        sp->head =  new_point;
        new_point->next = temp_1;
        return 1;
        
    }
    
    else{
        
        struct points*temp = sp->head;
        struct points*temp1 = temp->next;
        
		if(temp1 == NULL){
            temp->next =new_point;
            return 1;
            
        }
        
        while (temp1 != NULL) {
        
        if(temp1->distance == new_point->distance){
                if(temp1->x > (new_point->x)){
                    temp->next = new_point;
                    new_point->next = temp1;
                    return 1;
                }
                if (temp1->x <= new_point->x || temp1->y < new_point->y) {
                    struct points* temp3 = temp1->next;
                    temp1->next = new_point;
                    new_point->next = temp3;
                    return 1;
                }
                else{
                    temp->next = new_point;
                    new_point->next = temp1;
                    return 1;
                }
                
            }
            
            
        
        while(temp1->distance <(new_point->distance)){
            temp = temp1 ;
            
            temp1 = temp1->next ;
            if (temp1==NULL) {
                break;
            }
           
        
        }
            break;
        }
        
        
        
        
        
        
        temp->next = new_point;
        new_point->next = temp1;
        return 1;
        
        
    }
    
    return 0;
}

int
sp_remove_first(struct sorted_points *sp, struct point *ret)
{
    if (sp->head == NULL) {
        return 0;
    }
    struct points *temp = sp->head->next;
    ret->x = sp->head->x;
    ret->y = sp->head->y;
    free(sp->head);
    sp->head = temp ;
    return 1;
}

int
sp_remove_last(struct sorted_points *sp, struct point *ret)
{
    struct points * temp = sp->head;
    
    if (temp == NULL) {
        return 0;
    }
    struct points *temp1 = temp->next;
    
    if (temp1==NULL) {
        ret->x = temp->x;
        ret->y = temp->y;
        free(temp);
        sp->head = NULL;
        return 1;
    }
    while(temp1->next != NULL){
        temp1 = temp1->next;
        temp = temp->next;
    }
    ret->x = temp1->x;
    ret->y =temp1->y;
    free(temp1);
    temp->next = NULL;
    return 1;
    
}

int
sp_remove_by_index(struct sorted_points *sp, int index, struct point *ret)
{
    int count= 1;
    struct points *temp = sp->head ;
    if(temp ==NULL){
        return 0;
    }

	if(index ==0){
	sp->head =sp->head->next;
	ret->x = temp->x;
	ret->y = temp->y;
	free(temp);
	
	temp = NULL;
	return 1;
	}
    struct points *temp1 = temp ->next;
    
    if (temp1 == NULL && index>0) {
        return 0;
    }
    
    while(temp1->next!= NULL){
        temp1 = temp1 ->next ;
        count = count+ 1;
    }
    
    if(count< index){
        return 0;
    }
    count =0 ;
    temp1 = temp->next ;
   	
    while(count <index){
        temp = temp->next;
        temp1 = temp1->next;
        count = count + 1;
    }
    struct points* temp2 = sp->head;
    count = 0 ;
    while(count < index-1){
        temp2 = temp2->next;
        count = count + 1;
    }
    
    ret->x = temp->x;
    ret->y =temp->y ;
    free(temp);
	temp = NULL;

	if(temp2 ==NULL){
		return 1;

	}
    temp2->next = temp1;
    
    
    return 1;
    
    
    
    
    
}

int
sp_delete_duplicates(struct sorted_points *sp)
{
    int count = 0;
    int index = 1;
    int index_counter = 1;
    int flag =0;
    int x ,y;
    struct point ret ;
    struct points * temp = sp->head ;
    struct points * temp_1;
	//struct points *temp_2 = temp;
    while (temp!= NULL) {
		
        x = temp->x ;
        y = temp->y ;
		
        temp_1 = temp ->next;
        //temp_2 = temp ;
        while(temp_1 != NULL){

			if(temp_1 ==NULL){
					flag = sp_remove_by_index(sp,index,&ret);
						return count;
				}
			
			
            if(temp_1->x == x&& temp_1->y ==y){
                count = count + 1;
                temp_1 = temp_1 ->next;
				//temp->next = temp_1;
				index = index +1 ;
				
                
                flag = sp_remove_by_index(sp,index,&ret);
				
				index = index -1;
                if(index >index_counter){
                    flag = 0;
                }
                
            
            }
            else{
           
               break;
            }
        }
        temp = temp_1 ;
        
        if(flag ==1){index_counter = index_counter + 1;}
        flag = 0;
        index = index_counter;
    }
    
    return count;
}
