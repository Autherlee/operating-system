#include <assert.h>
#include <stdlib.h>
#include <ucontext.h>
#include "thread.h"
#include "interrupt.h"
#include "stdint.h"

//#include "readyque.h"
/* This is the thread control block */

struct thread {
    Tid ID;
    int state;
    ucontext_t* mycontext;
    unsigned int *sp;
   unsigned int *sp_for_free;
    int dead;
    //int somebody_wantkillu;
    //struct thread * prev;
    struct thread * next;
    
};


/* This is the wait queue structure */
struct wait_queue {
	/* ... Fill this in ... */
    struct thread* head;
};


struct rdq{
    int size ;
    
    struct thread* active;
    struct thread* head;
    
}rdq;


static struct rdq* my_rdq ;
static int current_thread_list[THREAD_MAX_THREADS];

struct thread* for_free;






int rdq_init(struct rdq** new_rdq){
    
    *new_rdq = malloc(sizeof(rdq));
    if (new_rdq == NULL) {
        return 0;
    }
    
    (*new_rdq)->size =0;
    
    (*new_rdq) ->active = NULL;
    
    (*new_rdq) ->head =NULL;
    
    return 1;

    
    }

struct thread* first_to_run(){
    
    if (my_rdq->head == NULL) {
        return  NULL;
    }
    
    struct thread* temp =my_rdq->head;
    
   

    my_rdq->head =my_rdq->head->next;
     

	temp->next = NULL;
    my_rdq->size --;
    
    return temp;
    
    
}



struct thread * pop_from_rdq(struct rdq* my_que ,Tid id){
    
        if (my_que->head ==NULL)
            return NULL;
    
   		
        struct thread* temp =my_que->head;
    
        if (temp->ID==id)
        
        {
            temp->next = NULL;
            my_rdq->head =my_rdq->head->next;
            
            my_rdq->size --;
            
            return temp;
            
       
        }
        struct thread* temp1 = my_que->head;
    
        struct thread* temp2 = temp1->next;
    
        while (temp2 != NULL) {
        
            if (temp2->ID == id) {
            
                temp1->next = temp2->next;
            
                temp2->next =NULL;
                
                my_que->size --;
                return temp2 ;
            }
            temp1= temp1->next;
            temp2 = temp2->next;
            
        }
    
    
        return NULL;
            
    
    }









int add_new_thread(struct thread * new_thread , struct rdq * myque){
    
    struct thread * temp  = myque ->head ;
    
	myque ->size ++ ;
    
	//printf("size:%d\n",myque->size);
    if (temp == NULL) {
        
	myque->head = new_thread;
       // new_thread ->prev =  NULL;
        new_thread ->next = NULL;
        return 1;
    }
    
    while(temp->next!= NULL){
        temp = temp ->next ;
        
    }
    
    temp ->next = new_thread ;
    
    new_thread ->next = NULL ;
    
    return 1;
    
    
    }





int add_new_thread_wq(struct thread * new_thread , struct wait_queue * myque){
    
    struct thread * temp  = myque ->head ;
    
	//myque ->size ++ ;
    
	//printf("size:%d\n",myque->size);
    if (temp == NULL) {
        
	myque->head = new_thread;
       // new_thread ->prev =  NULL;
        new_thread ->next = NULL;
        return 1;
    }
    
    while(temp->next!= NULL){
        temp = temp ->next ;
        
    }
    
    temp ->next = new_thread ;
    
    new_thread ->next = NULL ;
    
    return 1;
    
    
    }









struct thread * find(int id){
    
    struct thread* temp = my_rdq->head;
    if (temp==NULL) {
        return  NULL;
    }
    
    while (temp != NULL) {
        if (temp->ID == id) {
            return temp;
        }
        temp =temp->next;
        
    }
    return NULL;
}





int destory_que( struct rdq * que){
    
    free(que) ;
    que = NULL ;
    
    return 1;
    
}



    
    
    
    





void
thread_stub(void (*thread_main)(void *), void *arg)
{
	
    int enabled = interrupts_set(1);
	
    Tid ret;


 
    thread_main(arg); // call thread_main() function with arg
    
	
    ret = thread_exit(THREAD_SELF);
   
    // we should only get here if we are the last thread.
    assert(ret == THREAD_NONE);
    
    free(my_rdq->head->sp_for_free);
    free(my_rdq->head);
    destory_que( my_rdq) ;
    
	interrupts_set(enabled);
    // all threads are done, so process should exit
    exit(0);
}


void
thread_init(void)
{
    int check ;
	int i =0 ;
    for (i =0; i<THREAD_MAX_THREADS; i++) {
        current_thread_list[i] = 0;
    }
    
	check  = rdq_init(&my_rdq) ;
    
	assert(check);
    
	struct thread *kernal  = malloc(sizeof(struct thread));
	
	kernal->mycontext = malloc(sizeof(ucontext_t));
	assert(kernal!= NULL);
   
    kernal ->ID = 0;
    kernal-> state = 1;
   kernal ->dead = 0;
	
    if(check ==1){
        check = check -1;
    }
    my_rdq->active = kernal ;
    my_rdq->head = NULL;
	my_rdq->size ++ ;
}

Tid
thread_id()
{
	
	return my_rdq->active->ID;
}

Tid
thread_create(void (*fn) (void *), void *parg)
{
    int enabled = interrupts_set(0);

	//printf("1\n");

    if(my_rdq ->size == THREAD_MAX_THREADS-1){
        
        interrupts_set(enabled);
        return THREAD_NOMORE ;
        
    }
    
    
    struct thread* new_thread = malloc(sizeof(struct thread)) ;
	
    
    new_thread ->sp = malloc(THREAD_MIN_STACK+8) ;
    
	new_thread ->mycontext = malloc(sizeof(ucontext_t));

    if (new_thread->sp == NULL) {
        
        free(new_thread) ;
        interrupts_set(enabled);
        return THREAD_NOMEMORY;
    }
	

		
		//int check;
	getcontext((new_thread->mycontext));
	
	  
	
    new_thread->sp_for_free = new_thread->sp ;
    
    new_thread ->sp = new_thread->sp + THREAD_MIN_STACK/4 + 2;
    
   //(new_thread->sp)-- ;
    
   //*(new_thread->sp) = (uintptr_t) parg;
    
   //(new_thread->sp)-- ;
    
  // *(new_thread->sp) = (uintptr_t) fn;
	
   //(new_thread->sp) = new_thread->sp + 2;
	
    (new_thread->mycontext)->uc_mcontext.gregs[REG_RIP] = (uintptr_t)thread_stub;
	 
	(new_thread->mycontext)->uc_mcontext.gregs[REG_RDI] =(uintptr_t) fn;

	(new_thread->mycontext)->uc_mcontext.gregs[REG_RSI] =(uintptr_t) parg;
    (new_thread->mycontext)->uc_mcontext.gregs[REG_RSP] =(uintptr_t) new_thread->sp;
    
	//(new_thread->mycontext).uc_stack.ss_sp =new_thread->sp;
    


    int i =0;
    
    while( current_thread_list [i] == i){
        i ++ ;
        
    }
    current_thread_list[i] = i;
    
    
    new_thread->ID =i;
    
    new_thread->state = 0;
    
    new_thread->dead =0;
    
    
    add_new_thread(new_thread, my_rdq);
    
    
    
    
    
    interrupts_set(enabled);
    return i;
}




Tid
thread_yield(Tid want_tid)
{
    
    //printf("lol\n");
    int enabled = interrupts_set(0);
   	
	
    	
	
    int ret_id;
    
    struct thread * temp = NULL;
	//int check_done = 0; 
	
	
		
    
    if (want_tid == THREAD_SELF ||want_tid ==my_rdq->active->ID ) {
        
        ret_id = my_rdq->active->ID;
        
        interrupts_set(enabled);
        return ret_id;
    }
	

	
    
    
    if (want_tid > THREAD_MAX_THREADS || want_tid < -2) {
        interrupts_set(enabled);
        return THREAD_INVALID ;
    }

    
	if(want_tid >= 0 && current_thread_list[want_tid] != want_tid){
        interrupts_set(enabled);
		return THREAD_INVALID ;
	}
    
    
    
    
    if(my_rdq->head== NULL){
        interrupts_set(enabled);
        return THREAD_NONE;
    }
    
    
    
    if (want_tid == THREAD_ANY) {
        
        
		
     
	
		int check_done = 0;

        temp = first_to_run();
		int check = getcontext( (my_rdq->active->mycontext));	
		
		assert(check ==0);

		
    	



 		

		ret_id = temp->ID;
        if (my_rdq->active->dead == 1) {
           check= thread_exit(THREAD_SELF);
            assert(check);
        }
		
        if(!check_done){
		
        my_rdq ->active->state  = 0;
       
		struct thread* temp1 = my_rdq->active;
        add_new_thread(temp1, my_rdq);
        
        my_rdq->active = temp;
        
        my_rdq->active->state  =1 ;
		
		check_done =1;
		
         int err = setcontext( (temp->mycontext));
        assert(!err) ;
        }
             interrupts_set(enabled);
		 return ret_id;

        }
    
        
    
    temp = pop_from_rdq(my_rdq, want_tid);
    
   
    if (temp == NULL) {
        
         interrupts_set(enabled);
        return THREAD_INVALID ;
    } // should not be NULL, But just in case
    
	int check_done = 0;

    
	int check = getcontext( (my_rdq->active->mycontext));	
		
	assert(check ==0);
    

	
		
    	






    ret_id = want_tid;

    if (my_rdq->active->dead == 1) {
        check= thread_exit(THREAD_SELF);
        assert(check);
    }
    
    
    
	if(!check_done){
	my_rdq->active->state = 0;
    
    add_new_thread(my_rdq->active, my_rdq);
    
    my_rdq -> active = temp ;
	
	my_rdq->active->state = 1;
	
    check_done =1;
    
    int err = setcontext( (my_rdq->active->mycontext));
    assert(!err) ;
		
	}
    
     interrupts_set(enabled);
    
	return want_tid;
}









Tid
thread_exit(Tid tid)
{

	
    int enabled = interrupts_set(0);
  

	
    int ret_id;
    
   
    if(tid == THREAD_ANY){
        

    	
	
        if (my_rdq->head ==NULL) {
            return THREAD_NONE;
        }
        
        my_rdq->head->dead =1;
       
        ret_id = my_rdq->active->ID;
        
        interrupts_set(enabled);
		
        return ret_id;
        
	        
    }
    
    
    if (tid ==THREAD_SELF || tid == my_rdq->active->ID) {
        
        if (my_rdq ->head ==NULL) {
             interrupts_set(enabled);
            return THREAD_NONE ;
        }
        
        my_rdq->active->dead = 1;
		
        
        current_thread_list[my_rdq->active->ID] =0;
        
        struct thread* temp = first_to_run();
        
		
        
		for_free = my_rdq->active;

			
		
        my_rdq->active = temp ;
        

		free(for_free->mycontext);

        if (my_rdq->active->dead == 1) {
            thread_exit(THREAD_SELF);
        }

		my_rdq->size --;
	
	
        ret_id = my_rdq->active->ID;
        
        setcontext((my_rdq->active->mycontext));
        
        interrupts_set(enabled);
        
        return ret_id;
    }
    
    
    struct thread * temp = find( tid);
    
    if(!temp){
         interrupts_set(enabled);
        return THREAD_INVALID;
    }
    
    
    

        temp->dead = 1;
		
		current_thread_list[temp->ID] =0;
  
        ret_id = temp->ID;
    
    
    
     interrupts_set(enabled);
   
    
    return ret_id;
}

/*******************************************************************
 * Important: The rest of the code should be implemented in Lab 3. *
 *******************************************************************/


struct wait_queue *
wait_queue_create()
{
	struct wait_queue *wq;

	wq = malloc(sizeof(struct wait_queue));
	assert(wq);

    wq->head =NULL;

	return wq;
}

void
wait_queue_destroy(struct wait_queue *wq)
{
    if (wq->head==NULL) {
        free(wq);
    }
	
}

Tid
thread_sleep(struct wait_queue *queue)
{
    
    int enabled = interrupts_set(0);
   
	if(queue ==NULL){
		interrupts_set(enabled);
		return THREAD_INVALID;
	}


    if (my_rdq->head == NULL) {
       
		interrupts_set(enabled);
		 return  THREAD_NONE;
    }
    
    struct thread* temp = first_to_run() ;
    
    int check_done =0;
    
    getcontext(my_rdq->active->mycontext);
    
    if (my_rdq->active->dead==1) {
        thread_exit(THREAD_SELF);
    }
    
    
    if (!check_done) {
        
        check_done =1;
        
        add_new_thread_wq(my_rdq->active, queue);
      
        
        
        
        my_rdq->active =temp;
        
        
        setcontext(temp->mycontext);
        
        
    }
    int ret_it = my_rdq->active->ID;
    
    interrupts_set(enabled);
    
	return ret_it;
}

/* when the 'all' parameter is 1, wakeup all threads waiting in the queue.
 * returns whether a thread was woken up on not. */
int
thread_wakeup(struct wait_queue *queue, int all)
{
    
    int enabled = interrupts_set(0);
   
    int number = 0;
    
    if (queue==NULL) {
        
        interrupts_set(enabled);
        return 0;
        
    }
    
    if (queue->head ==NULL) {
        interrupts_set(enabled);
        return 0;
    }
    
    
    if (!all) {
        
        struct thread* temp = queue->head;
        
        queue->head = temp->next;
        
        temp->next = NULL ;
        
        add_new_thread(temp, my_rdq);
        
        interrupts_set(enabled);
        return 1;
    
        
    }
    
    struct thread* temp = queue->head ;
    
    while (temp != NULL) {
        
        queue->head = queue->head->next;
        
        add_new_thread(temp, my_rdq);
        
        number++;
        
        temp = queue->head;

    }
    
    
    
    
    interrupts_set(enabled);
    
    return number;
    
    
	
}



struct lock {
	/* ... Fill this in ... */
    
    struct wait_queue* my_wq;
    
    int active_thread;
    
    int state;
    
};


struct lock *
lock_create()
{
    
    int enabled = interrupts_set(0);
    
    struct lock* my_lock = malloc(sizeof(struct lock));
    
    my_lock->active_thread = -100;
    
    my_lock->my_wq = wait_queue_create();
    
    my_lock->state = 0;

    
    interrupts_set(enabled);
	
    return my_lock;
}

void
lock_destroy(struct lock *lock)
{
    
    int enabled = interrupts_set(0);
    
	assert(lock != NULL);

    if (lock->state == 0 && lock->my_wq ==NULL) {
        free(lock);
    }

	
    
    
    interrupts_set(enabled);
}



void
lock_acquire(struct lock *lock)
{
    int enabled = interrupts_set(0);
	
    assert(lock != NULL);
    
    if (lock->state ==0) {
        lock->state =1;
        lock->active_thread = my_rdq->active->ID;
        interrupts_set(enabled);
        return;
    }
    
    while(lock->state==1 && lock->active_thread != my_rdq->active->ID){
        
        thread_sleep(lock->my_wq);
 
    

    }
    
    lock->state =1;
    
    lock->active_thread = my_rdq->active->ID;
    
    interrupts_set(enabled);
    return ;
    
}

void
lock_release(struct lock *lock)
{
    int enabled = interrupts_set(0);
	assert(lock != NULL);

    if (lock->active_thread== my_rdq->active->ID && lock->state == 1) {
        
        lock->active_thread = -1;
        lock->state = 0;
       
        thread_wakeup(lock->my_wq, 1);
       
        
    }
    
    //lock->active_thread = lock->my_wq->head->ID;
    
   
    
    
    interrupts_set(enabled);
    return ;
    
    
}



struct cv {
	/* ... Fill this in ... */
    
    struct wait_queue * my_wq;
};

struct cv *
cv_create()
{
    int enabled = interrupts_set(0);
	struct cv *cv;

	cv = malloc(sizeof(struct cv));
	assert(cv);

    cv->my_wq = wait_queue_create();

    interrupts_set(enabled);
	return cv;
}

void
cv_destroy(struct cv *cv)
{
    
    int enabled = interrupts_set(0);
	assert(cv != NULL);
    
    if(cv->my_wq == NULL){
     
        
        free(cv);
    }
	
    interrupts_set(enabled);
    return;

}

void
cv_wait(struct cv *cv, struct lock *lock)
{
    int enabled = interrupts_set(0);
	assert(cv != NULL);
	assert(lock != NULL);

    if (my_rdq->active->ID== lock->active_thread && lock->state ==1) {
      
        
        lock_release( lock);
        
        thread_sleep(cv->my_wq);
        
        lock_acquire(lock);
    }
    
    
    
    
    interrupts_set(enabled) ;
    
    return;
    
}



void
cv_signal(struct cv *cv, struct lock *lock)
{
    
    int enabled = interrupts_set(0);
	assert(cv != NULL);
	assert(lock != NULL);
    if (my_rdq->active->ID== lock->active_thread && lock->state ==1) {
        
        thread_wakeup(cv->my_wq, 0);
        
    }
        
    
    
    
    
    interrupts_set(enabled);
    return ;
    
}




void
cv_broadcast(struct cv *cv, struct lock *lock)
{
    int enabled = interrupts_set(0);
    assert(cv != NULL);
    assert(lock != NULL);
    if (my_rdq->active->ID== lock->active_thread && lock->state ==1) {
        
        thread_wakeup(cv->my_wq, 1);
        
    }
    
    
    
    
    
    interrupts_set(enabled);
    return ;
}
