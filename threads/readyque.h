//
//  readyque.h
//  os2
//
//  Created by yu li on 2015-10-08.
//  Copyright © 2015 Auther. All rights reserved.
//

#ifndef readyque_h
#define readyque_h

#include <stdio.h>
#include <stdlib.h>
#include "thread.c"




struct rdq{
    int size ;
    
    struct thread* active;
    struct thread* head;
    
}rdq;


int rdq_init(struct rdq* new_rdq);

struct thread * find(struct rdq* my_que , Tid id);

int add_new_thread(struct thread * new_thread, struct rdq * myque);

int destory_que(struct rdq * que);



#endif /* readyque_h */
