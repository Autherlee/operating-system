//
//  readyque.c
//  os2
//
//  Created by yu li on 2015-10-08.
//  Copyright © 2015 Auther. All rights reserved.
//

#include "readyque.h"






int rdq_init( struct rdq* new_rdq){
    
    new_rdq = malloc(sizeof(struct rdq));
    if (new_rdq == NULL) {
        return 0;
    }
    
    new_rdq ->size =0;
    
    new_rdq ->active = NULL;
    
    new_rdq ->head =NULL;
    
    return 1;

    
    }


struct thread * find(struct rdq* my_que ,Tid id){
    
    struct thread * temp = my_que ->head ;
    
    if (temp == NULL){
        return NULL ;
    }
    
    while(temp->ID != id && temp->next != NULL){
        
        temp = temp ->next ;
    }
    if(temp->ID == id){
        return temp ;
    }
    return NULL ;
    }


int add_new_thread(struct thread * new_thread , struct rdq * myque){
    
    struct thread * temp  = myque ->head ;
    myque ->size ++ ;
    
    if (temp == NULL) {
        myque->head = new_thread;
        new_thread ->prev =  NULL;
        new_thread ->next = NULL;
        return 1;
    }
    
    while(temp->next!= NULL){
        temp = temp ->next ;
        
    }
    
    temp ->next = new_thread ;
    
    new_thread ->next = NULL ;
    new_thread -> prev = temp;
    
    return 1;
    
    
    }


int destory_que( struct rdq * que){
    
    free(que) ;
    que = NULL ;
    
    return 1;
    
}

