#include <assert.h>
#include <stdlib.h>
#include <ucontext.h>
#include "thread.h"
#include "interrupt.h"
#include "stdint.h"

//#include "readyque.h"
/* This is the thread control block */

struct thread {
    Tid ID;
    int state;
    ucontext_t mycontext;
    unsigned int *sp;
    unsigned int *sp_for_free;
    int dead;
    int somebody_wantkillu;
    //struct thread * prev;
    struct thread * next;
    
};




struct rdq{
    int size ;
    
    struct thread* active;
    struct thread* head;
    
}rdq;


static struct rdq* my_rdq ;

static struct rdq my_wq;

static int current_thread_list[THREAD_MAX_THREADS];




struct thread* remove_first_in_wq(){
    
    struct thread* temp = my_wq.head ;
    if(temp == NULL || temp->next ==NULL){
        my_wq.head =NULL;
        my_wq.size =0;
        return temp;
    }
    
    my_wq.head = my_wq.head->next;
    
    temp->next = NULL;
    
    return temp;
    
}


void add_thread_wq(struct thread* blocked){
    
    struct thread *temp = my_wq.head;
    
    if (temp== NULL) {
        my_wq.head = blocked;
        my_wq.head->next = NULL;
        return ;
    }
    
    while (temp->next != NULL) {
        temp = temp->next ;
    }
    
    temp->next = blocked;
    
    blocked->next = NULL;
    return ;
    
    
    
    
    
}





int rdq_init(struct rdq** new_rdq){
    
    *new_rdq = malloc(sizeof(rdq));
    if (new_rdq == NULL) {
        return 0;
    }
    
    (*new_rdq)->size =0;
    
    (*new_rdq) ->active = NULL;
    
    (*new_rdq) ->head =NULL;
    
    my_wq.active =NULL;
    my_wq.head  =NULL;
   
    my_wq.size=0;
    
    
    return 1;

    
    }


struct thread * find(struct rdq* my_que ,Tid id){
    
    struct thread * temp = my_que ->head ;
    
    if (temp == NULL){
        return NULL ;
    }
    
    if (temp->ID == id) {
        
        my_que->head = my_que->head->next;
	
		my_que->size--;
        return temp;
    }
    
    
    struct thread* temp1 = temp ;
    
    
    temp = temp->next;
    
    if (temp ==NULL) {
        return NULL;
    }
    
    while(temp->ID != id && temp->next != NULL){
        
        temp1 =temp ;
        temp = temp ->next ;
        
    }
    
    
    if(temp->ID == id){
        temp1->next = temp->next;
		my_que->size--;
        return temp ;
    }
    
    
    return NULL ;
    }


int add_new_thread(struct thread * new_thread , struct rdq * myque){
    
    struct thread * temp  = myque ->head ;
    myque ->size ++ ;
    
    if (temp == NULL) {
        myque->head = new_thread;
       // new_thread ->prev =  NULL;
        new_thread ->next = NULL;
        return 1;
    }
    
    while(temp->next!= NULL){
	
		 if(temp->ID >= temp->next->ID){
			temp->next = NULL;
			break;
		}	
        temp = temp ->next ;
        
    }
    
    temp ->next = new_thread ;
    
    new_thread ->next = NULL ;
    //new_thread -> prev = temp;
    
    return 1;
    
    
    }


int add_new_thread_t(struct thread * new_thread , struct rdq * myque){
    
    struct thread * temp  = myque ->head ;
  
    
    if (temp == NULL) {
        myque->head = new_thread;
       // new_thread ->prev =  NULL;
        new_thread ->next = NULL;
        return 1;
    }
    
    while(temp->next!= NULL){
	
		 if(temp->ID >= temp->next->ID){
			temp->next = NULL;
			break;
		}	
        temp = temp ->next ;
        
    }
    
    temp ->next = new_thread ;
    
    new_thread ->next = NULL ;
    //new_thread -> prev = temp;
    
    return 1;
    
    
    }



int destory_que( struct rdq * que){
    
    free(que) ;
    que = NULL ;
    
    return 1;
    
}



void kill_dead_thread(){
    
    struct thread *temp = my_rdq->head;
   
    if (my_rdq->head ==NULL) {
        return;
    }
    //printf("called2");
    if(temp->dead ==1){
        
        my_rdq->head = temp->next;
        free(temp->sp_for_free);
        free(temp);
        return;
    }
    
    struct thread* temp1 = temp->next ;
    while(temp->next != NULL){
       
        temp1 = temp->next;
		
	
		
        if(temp1->dead ==1){
            temp->next =temp1->next;
            free(temp1->sp_for_free);
            free(temp1);
            return ;
   			 }

		temp = temp->next;
    }
    
    return;
    
}
    
    
    






void
thread_stub(void (*thread_main)(void *), void *arg)
{
	
     interrupts_set(1);
	
    Tid ret;

   	if(my_rdq->active->dead != 1){
    kill_dead_thread();}

    if (my_rdq->active->somebody_wantkillu ==1) {
        int check= thread_exit(THREAD_SELF);
        assert(check);
    }
    thread_main(arg); // call thread_main() function with arg
    
    ret = thread_exit(THREAD_SELF);
   
    // we should only get here if we are the last thread.
    assert(ret == THREAD_NONE);
    
    free(my_rdq->head->sp_for_free);
    free(my_rdq->head);
    destory_que( my_rdq) ;
   //interrupts_set(enabled);

	
    // all threads are done, so process should exit
    exit(0);
}


void
thread_init(void)
{
    int check ;
	int i =0 ;
    for (i =0; i<THREAD_MAX_THREADS; i++) {
        current_thread_list[i] = 0;
    }
    
	check  = rdq_init(&my_rdq) ;
    
	assert(check);
    
	struct thread *kernal  = malloc(sizeof(struct thread));
	
	assert(kernal!= NULL);
   
    kernal ->ID = 0;
    kernal-> state = 1;
   kernal ->dead = 0;
	kernal->somebody_wantkillu =0;
    if(check ==1){
        check = check -1;
    }
    my_rdq->active = kernal ;
    my_rdq->head = NULL;
	my_rdq->size ++ ;
}

Tid
thread_id()
{
	
	return my_rdq->active->ID;
}


Tid
thread_create(void (*fn) (void *), void *parg)
{
    int enabled = interrupts_set(0);
    	if(my_rdq->active->dead != 1){
    kill_dead_thread();}
   // kill_dead_thread();


    if(my_rdq ->size == THREAD_MAX_THREADS){
        
        interrupts_set(enabled);
        return THREAD_NOMORE ;
        
    }
    
    
    struct thread* new_thread = malloc(sizeof(struct thread)) ;
	
    
    new_thread ->sp = malloc(THREAD_MIN_STACK+8) ;
    
    if (new_thread->sp == NULL) {
        
        free(new_thread) ;
        interrupts_set(enabled);
        return THREAD_NOMEMORY;
    }
	

		
		//int check;
	int check = getcontext(&(new_thread->mycontext));
	
	  
	
    new_thread->sp_for_free = new_thread->sp ;
    
    new_thread ->sp = new_thread->sp + THREAD_MIN_STACK/4 +2;
    
   //(new_thread->sp)-- ;
    
   //*(new_thread->sp) = (uintptr_t) parg;
    
   //(new_thread->sp)-- ;
    
  // *(new_thread->sp) = (uintptr_t) fn;
	
   //(new_thread->sp) = new_thread->sp + 2;
	
    (new_thread->mycontext).uc_mcontext.gregs[REG_RIP] = (uintptr_t)thread_stub;
	 
	(new_thread->mycontext).uc_mcontext.gregs[REG_RDI] =(uintptr_t) fn;

	(new_thread->mycontext).uc_mcontext.gregs[REG_RSI] =(uintptr_t) parg;
    (new_thread->mycontext).uc_mcontext.gregs[REG_RSP] =(uintptr_t) new_thread->sp;
    
	//(new_thread->mycontext).uc_stack.ss_sp =new_thread->sp;
    


    int i =0;
    
    while( current_thread_list [i] == i){
        i ++ ;
        
    }
    current_thread_list[i] = i;
    
    
    new_thread->ID =i;
    
    new_thread->state = 0;
    
    new_thread->dead =0;
    
    new_thread->somebody_wantkillu =0;
    
    check = add_new_thread(new_thread, my_rdq);
   
    
    assert(check) ;
    
    
    
    
    
    interrupts_set(enabled);
    return i;
}




Tid
thread_yield(Tid want_tid)
{
    

    int enabled = interrupts_set(0);
    struct thread * temp = NULL;
	//int check_done = 0; 
	//printf("called 2\n");
	
		

	

	if(my_rdq->active->dead != 1){
    kill_dead_thread();}

    
    if (want_tid == THREAD_SELF ||want_tid ==my_rdq->active->ID ) {
        
        int ret_id_active =  my_rdq->active->ID;
        interrupts_set(enabled);
        return ret_id_active;
    }
	

	
    
    
    if (want_tid > THREAD_MAX_THREADS || want_tid < -2) {
        interrupts_set(enabled);
        return THREAD_INVALID ;
    }

	if(want_tid >= 0 && current_thread_list[want_tid] != want_tid){
        interrupts_set(enabled);
		return THREAD_INVALID ;
	}
    
    
    if( my_rdq->head == NULL ){
        
        interrupts_set(enabled);
        
        return THREAD_NONE;
    }
    
    
    if (want_tid == THREAD_ANY) {
        
        
		temp = my_rdq->head;
        
        my_rdq->head = my_rdq->head->next;
        
        temp->next =NULL;
        
       
		int check_done = 0;
		
		if(my_rdq->active->dead != 1){
    kill_dead_thread();}


		int check = getcontext( &(my_rdq->active->mycontext));	
		
		assert(check ==0);
		
       if(my_rdq->active->dead != 1){
    kill_dead_thread();}

        if (my_rdq->active->somebody_wantkillu ==1&&my_rdq->active->dead != 1) {
           check= thread_exit(THREAD_SELF);
            assert(check);
        }
		
        if(!check_done){
		 add_new_thread_t(my_rdq->active, my_rdq);
		
        
        my_rdq ->active->state  = 0;
       
        my_rdq->active = temp;
        
        my_rdq->active->state  =1 ;
		
		check_done =1;
		
         int err = setcontext( &(temp->mycontext));
        assert(!err) ;
        }
	
		int ret_id_active =  my_rdq->active->ID;
             interrupts_set(enabled);
		 return ret_id_active;

        }
    
        
    
    temp = find(my_rdq,want_tid);
    
   
    if (temp == NULL) {
        
         interrupts_set(enabled);
        return THREAD_INVALID ;
    } // should not be NULL, But just in case
    
	int check_done = 0;
	if(my_rdq->active->dead != 1){
    kill_dead_thread();}

	int check = getcontext( &(my_rdq->active->mycontext));	
		
	assert(check ==0);

  if(my_rdq->active->dead != 1){
    kill_dead_thread();}

    if (my_rdq->active->somebody_wantkillu ==1&&my_rdq->active->dead != 1) {
        check= thread_exit(THREAD_SELF);
        assert(check);
    }
    
    
    
	if(!check_done){
        my_rdq->active->state = 0;
        
        add_new_thread_t(my_rdq->active, my_rdq);
        
        my_rdq -> active = temp ;
	
        my_rdq->active->state = 1;
	check_done =1;    
    int err = setcontext( &(my_rdq->active->mycontext));
    assert(!err) ;
		
	}
    
     interrupts_set(enabled);
    
	return want_tid;
}









Tid
thread_exit(Tid tid)
{
    int enabled = interrupts_set(0);
   // kill_dead_thread() ;
	//printf(" thread exit tid %d\n",tid);
    int ret_id = -1337;
    
    if(tid == THREAD_ANY){
        
			if(my_rdq->active->dead != 1){
    kill_dead_thread();}

        struct thread *temp = my_rdq->head;
       
		if(my_rdq->active->ID != temp->ID){
        ret_id = temp->ID;
		  temp->somebody_wantkillu =1;
        
        }
      // printf("current thread %d\n", my_rdq->active->ID);
		 if(temp->next!= NULL){
		temp = temp->next;
        temp->somebody_wantkillu =1;
//printf("thread exit temp  id%d\n",temp->ID);

			int red = temp->ID;
          interrupts_set(enabled);
			
        return red;}

		


		 interrupts_set(enabled);
		return THREAD_NONE;		
	        
    }
    
    
    if (tid ==THREAD_SELF) {
        
        if (my_rdq ->head->next ==NULL) {
             interrupts_set(enabled);
            return THREAD_NONE ;
        }
        
        my_rdq->active->dead = 1;
		my_rdq->size --;
        current_thread_list[my_rdq->active->ID] =0;
        int check = thread_yield(THREAD_ANY);
        assert(check);
        
    }
    
    
    struct thread * temp = find(my_rdq, tid);
    
    if(!temp){
         interrupts_set(enabled);
        return THREAD_INVALID;
    }
    
    
    
    
    if (my_rdq->active->ID == tid) {
        temp->dead = 1;
		my_rdq->size --;
		current_thread_list[temp->ID] =0;
        int check = thread_yield(THREAD_ANY);
        assert(check);
        
    }
    
    
    
    
    
    
    temp->somebody_wantkillu =1;
	current_thread_list[temp->ID] =0;
    ret_id = temp->ID;
    
    
    
	//printf("thread exit ret id%d\n",temp->ID);
     interrupts_set(enabled);
    return ret_id;
}

/*******************************************************************
 * Important: The rest of the code should be implemented in Lab 3. *
 *******************************************************************/

/* This is the wait queue structure */
struct wait_queue {
	/* ... Fill this in ... */
    struct thread* head;
};

struct wait_queue *
wait_queue_create()
{
	struct wait_queue *wq;

	wq = malloc(sizeof(struct wait_queue));
	assert(wq);

    wq->head =NULL;

	return wq;
}

void
wait_queue_destroy(struct wait_queue *wq)
{

	free(wq);
}

Tid
thread_sleep(struct wait_queue *queue)
{
    
    int enabled = interrupts_set(0);

	//printf("called 1\n");
	if(queue==NULL){
	interrupts_set(enabled);
        return THREAD_INVALID;
        
        
    }
    
    
    
    struct thread * temp1 = my_rdq->active;
    //printf("called");
    while ((temp1 ->state ==1 || temp1->state ==6 )&& temp1->next!=NULL) {
        

		if(temp1 ->ID > temp1->next->ID){
			temp1= NULL;
			//printf("called");			
			break;

		}
		temp1 = temp1->next;
    }
    
	if(temp1==NULL || temp1->state ==1) {
		temp1 = my_rdq->head;
    
    while ((temp1 ->state ==1 || temp1->state ==6 )&& temp1->next!=NULL) {
        temp1 = temp1->next;
    }
    
	}    


    if(temp1->ID == my_rdq->active->ID){
        

        interrupts_set(enabled);
        return  THREAD_NONE;
    }
    
  
    
    struct thread* temp = queue->head;
    
	
	if(temp ==NULL){
		queue->head =my_rdq->active;

	}

	

    else{
	
	  while(temp->next!= NULL){
		if(temp->ID >= temp->next->ID){
		
		temp->next = NULL;
		break;
		}        
		temp = temp->next;
		
    }



	temp->next = my_rdq->active;
	}    
    int check_done = 0;
    
   int check = getcontext(&(my_rdq->active->mycontext));
  
	check = check-1;
    
    if(!check_done){
        
        
        
        my_rdq->active->state = 6; //6 means the thread is blocked
        my_rdq->active = temp1;
        my_rdq->active->state =1;
        
        if (my_rdq->active->somebody_wantkillu ==1) {
            
            thread_exit(THREAD_SELF);
        }
        
        
        check_done =1;
        
        
        check = setcontext(&(my_rdq->active->mycontext));
        assert(check);
    
    }
    
    int ret  = my_rdq->active->ID;
    interrupts_set(enabled);
	return ret;
}

/* when the 'all' parameter is 1, wakeup all threads waiting in the queue.
 * returns whether a thread was woken up on not. */
int
thread_wakeup(struct wait_queue *queue, int all)
{
    
    int enabled = interrupts_set(0);
    if (queue ==NULL || queue->head == NULL) {
        
        interrupts_set(enabled);
        return 0;
    }
    
    if (all ==0) {
        struct thread* temp = queue->head;
        queue->head = queue->head->next;
        temp->state=0;
        
        interrupts_set(enabled);
        return 1;
    }
    
    int ret = 0 ;
    
    while (queue->head!= NULL) {
        ret++;
        queue->head->state =0;
        queue->head = queue->head->next;
        
    }
    
    interrupts_set(enabled);
    
    return ret;
    
    
	
}



struct lock {
	/* ... Fill this in ... */
    
    struct wait_queue* my_wq;
    int active_thread;
    int state;
    
};


struct lock *
lock_create()
{
    
    int enabled = interrupts_set(0);
    
	struct lock *lock;

	lock = malloc(sizeof(struct lock));
	assert(lock);
    
    lock->my_wq =  wait_queue_create() ;
    
    lock->active_thread = -1;
	
    lock->state = 0;

    
    interrupts_set(enabled);
	return lock;
}

void
lock_destroy(struct lock *lock)
{
    
    int enabled = interrupts_set(0);
    
	assert(lock != NULL);

    if (lock->state == 0 && lock->my_wq ==NULL) {
        free(lock);
    }

	
    
    
    interrupts_set(enabled);
}

void
lock_acquire(struct lock *lock)
{
    int enabled = interrupts_set(0);
	assert(lock != NULL);
    
    if (lock->state ==0) {
        lock->state =1;
        lock->active_thread = my_rdq->active->ID;
        interrupts_set(enabled);
        return;
    }
    
    if(lock->my_wq->head == NULL){
        
        lock->my_wq-> head = my_rdq->active ;
        
       thread_sleep(lock->my_wq);
       
        interrupts_set(enabled);
        return ;
        
    }
    
    
}

void
lock_release(struct lock *lock)
{
    int enabled = interrupts_set(0);
	assert(lock != NULL);

    if (lock->my_wq->head ==NULL) {
        
        lock->active_thread = -1;
        lock->state = 0;
        interrupts_set(enabled);
        return ;
        
    }
    
    //lock->active_thread = lock->my_wq->head->ID;
    
    thread_wakeup(lock->my_wq, 1);
    
    
    interrupts_set(enabled);
    return ;
    
    
}

struct cv {
	/* ... Fill this in ... */
    
    struct wait_queue * my_wq;
};

struct cv *
cv_create()
{
    int enabled = interrupts_set(0);
	struct cv *cv;

	cv = malloc(sizeof(struct cv));
	assert(cv);

    cv->my_wq = wait_queue_create();

    interrupts_set(enabled);
	return cv;
}

void
cv_destroy(struct cv *cv)
{
    
    int enabled = interrupts_set(0);
	assert(cv != NULL);
    
    if(cv->my_wq->head == NULL){
        free(cv->my_wq);
        
        free(cv);
    }
	
    interrupts_set(enabled);
    return;

}

void
cv_wait(struct cv *cv, struct lock *lock)
{
	assert(cv != NULL);
	assert(lock != NULL);

	TBD();
}

void
cv_signal(struct cv *cv, struct lock *lock)
{
	assert(cv != NULL);
	assert(lock != NULL);

	TBD();
}

void
cv_broadcast(struct cv *cv, struct lock *lock)
{
	assert(cv != NULL);
	assert(lock != NULL);

	TBD();
}
