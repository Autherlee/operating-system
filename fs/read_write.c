#include "testfs.h"
#include "list.h"
#include "super.h"
#include "block.h"
#include "inode.h"

/* given logical block number, read the corresponding physical block into block.
 * return physical block number.
 * returns 0 if physical block does not exist.
 * returns negative value on other errors. */
static int
testfs_read_block(struct inode *in, int log_block_nr, char *block)
{
	int phy_block_nr = 0;

	assert(log_block_nr >= 0);
	
        
        if (log_block_nr < NR_DIRECT_BLOCKS) {
		
			phy_block_nr = (int)in->in.i_block_nr[log_block_nr];

		}
   
        else {
	
            
            log_block_nr -= NR_DIRECT_BLOCKS;

      
            if (log_block_nr >= NR_INDIRECT_BLOCKS) {

            log_block_nr -= NR_INDIRECT_BLOCKS;
            
            if (log_block_nr >= NR_INDIRECT_BLOCKS*NR_INDIRECT_BLOCKS)
            {
                return -EFBIG;
            }
            
            unsigned int location = 0 ;
            
            unsigned int index = 0 ;        
            
            if (in->in.i_dindirect > 0) {
               
                    read_blocks(in->sb, block, in->in.i_dindirect, 1);
                
                    index = log_block_nr/NR_INDIRECT_BLOCKS;
                    
                    if(index<=(unsigned int)log_block_nr){
                         
						location = ((unsigned int *)block)[index];
                        
                        
                    }
                    else{
                        
                        index = index +1;
                        
						 location = ((int *)block)[index];
                    }
                   
            
                if (location <= 0) {
                
                     return location;   
                }
                    
                if(location>0){
                    
                    read_blocks(in->sb, block, (off_t)location, 1);
                    
                    phy_block_nr = ((unsigned int*)block)[log_block_nr%NR_INDIRECT_BLOCKS];
                }
                
            }
        }

		

		if (in->in.i_indirect > 0&& log_block_nr <NR_INDIRECT_BLOCKS) {
			
                    read_blocks(in->sb, block, in->in.i_indirect, 1);
		
                    phy_block_nr = ((unsigned int*)block)[log_block_nr];
		}
	

	}



	if (phy_block_nr > 0) {
		read_blocks(in->sb, block, phy_block_nr, 1);
	}


    else {
		/* we support sparse files by zeroing out a block that is not
		 * allocated on disk. */
		bzero(block, BLOCK_SIZE);
	}
	return phy_block_nr;
}

int
testfs_read_data(struct inode *in, char *buf, off_t start, size_t size)
{
	char block[BLOCK_SIZE];
	long block_nr = start / BLOCK_SIZE;
	long block_ix = start % BLOCK_SIZE;
	int ret;
        
        unsigned long max_size = 343765975043;
	
        assert(buf);
	if (start + (off_t) size > in->in.i_size) {
		size = in->in.i_size - start;
	}
        
        
        
        
	if (block_ix + size > BLOCK_SIZE) {
            
        unsigned int f = 1;  
       
        if (size <= max_size){ 
            
            ret = testfs_read_block(in, block_nr, block);
            
            if(ret < 0) {
            
                return ret;
            
            }
            
            memcpy(buf, block + block_ix, BLOCK_SIZE - block_ix);
        
             long temp_size = size - BLOCK_SIZE + block_ix;
             
            
            
             while (temp_size > BLOCK_SIZE) {
            
                ret = testfs_read_block(in, block_nr + f, block);
                 
                if ( ret>= 0)
                
                
                {
                    
                    
                memcpy(buf + BLOCK_SIZE*f - block_ix, block, BLOCK_SIZE);
                 
                 temp_size = temp_size - BLOCK_SIZE;
                 
                 f++;
                
                }
                
                
                else{
                
                    return ret;
                
                }
                
                 
             }
        
             
             ret = testfs_read_block(in, block_nr + f, block);
             
             if (ret  < 0){
                 return ret;
             }
             memcpy(buf + BLOCK_SIZE*f - block_ix, block, temp_size);
        
             return size;
	}
        
        else{
            
            return -EFBIG;
            
        }
        
        }
        
        ret = testfs_read_block(in, block_nr, block);
        
	if (ret < 0)
        {
            return ret;
        }
	memcpy(buf, block + block_ix, size);
	/* return the number of bytes read or any error */
	return size;
}

/* given logical block number, allocate a new physical block, if it does not
 * exist already, and return the physical block number that is allocated.
 * returns negative value on error. */
static int
testfs_allocate_block(struct inode *in, int log_block_nr, char *block)
{
	int phy_block_nr;
	char indirect[BLOCK_SIZE];
	int indirect_allocated = 0;

	assert(log_block_nr >= 0);
	phy_block_nr = testfs_read_block(in, log_block_nr, block);

	/* phy_block_nr > 0: block exists, so we don't need to allocate it, 
	   phy_block_nr < 0: some error */
	if (phy_block_nr != 0)
		return phy_block_nr;

	/* allocate a direct block */
	if (log_block_nr < NR_DIRECT_BLOCKS) {
		assert(in->in.i_block_nr[log_block_nr] == 0);
		phy_block_nr = testfs_alloc_block_for_inode(in);
		if (phy_block_nr >= 0) {
			in->in.i_block_nr[log_block_nr] = phy_block_nr;
		}
		return phy_block_nr;
	}

	log_block_nr -= NR_DIRECT_BLOCKS;
        
              
       int num_allocated = 0;  
         
       char dinblock[BLOCK_SIZE];  
    
        if (log_block_nr >= NR_INDIRECT_BLOCKS) {
        
   
            
        log_block_nr = log_block_nr - NR_INDIRECT_BLOCKS;
        
        
        
        
        
         
        int check = log_block_nr/NR_INDIRECT_BLOCKS;
        
        if (in->in.i_dindirect != 0) {
            
              read_blocks(in->sb, dinblock, in->in.i_dindirect, 1);
            
           
        }
        
        if(in->in.i_dindirect==0){
            
            bzero(dinblock, BLOCK_SIZE);
            
            phy_block_nr = testfs_alloc_block_for_inode(in);
            
            if (phy_block_nr < 0) {
            
                return -ENOSPC;
            
            }
            
            in->in.i_dindirect = phy_block_nr;
            
            num_allocated = 1;
            
            
          
        
        }
        
       
        
        if (((int *)dinblock)[check] != 0) {
          
            read_blocks(in->sb, indirect, ((int *)dinblock)[check], 1);
            
        }
        
        
        
       if (((int *)dinblock)[check] == 0) {
            
            bzero(indirect, BLOCK_SIZE);
            
            phy_block_nr = testfs_alloc_block_for_inode(in);
            
            if (phy_block_nr < 0) {
               
                if (num_allocated == 1) {
             
                    testfs_free_block_from_inode(in, in->in.i_dindirect);
                    
                    return -ENOSPC;
                }
            }
            
            if(phy_block_nr>=0){
               
               
                
                ((int *)dinblock)[check] = phy_block_nr;
                
                indirect_allocated = 1;
                
                write_blocks(in->sb, dinblock, in->in.i_dindirect, 1);
            
            }
            
        
        
        }
        
        
        assert(((int *)indirect)[log_block_nr%NR_INDIRECT_BLOCKS] == 0);
        
        phy_block_nr = testfs_alloc_block_for_inode(in);
        
        if (phy_block_nr >= 0) {
              
          
            ((int *)indirect)[log_block_nr%NR_INDIRECT_BLOCKS] = phy_block_nr;
           
            write_blocks(in->sb, indirect, ((int *)dinblock)[check], 1);
            
          
        }
       
        if(phy_block_nr <0){
              
            if (indirect_allocated >0) {
            
                testfs_free_block_from_inode(in, ((int *)dinblock)[check]);
            
            }
            
            
            
            if (num_allocated > 0) {
                
                testfs_free_block_from_inode(in, ((int *)dinblock)[check]);
                
                testfs_free_block_from_inode(in, in->in.i_dindirect);
            }
            
        
            
            return -ENOSPC;
          
        
        }
        
             return phy_block_nr;
    }

        
        
        
 
        
        
        
        
        
        
	if (in->in.i_indirect == 0) {	/* allocate an indirect block */
		bzero(indirect, BLOCK_SIZE);
		phy_block_nr = testfs_alloc_block_for_inode(in);
		if (phy_block_nr < 0)
			return phy_block_nr;
		indirect_allocated = 1;
		in->in.i_indirect = phy_block_nr;
	}
    else {	/* read indirect block */
		read_blocks(in->sb, indirect, in->in.i_indirect, 1);
	}

	/* allocate direct block */
	assert(((int *)indirect)[log_block_nr] == 0);	
	phy_block_nr = testfs_alloc_block_for_inode(in);

	if (phy_block_nr >= 0) {
		/* update indirect block */
		((int *)indirect)[log_block_nr] = phy_block_nr;
		write_blocks(in->sb, indirect, in->in.i_indirect, 1);
	} else if (indirect_allocated) {
		/* free the indirect block that was allocated */
		testfs_free_block_from_inode(in, in->in.i_indirect);
	}
	return phy_block_nr;
}

int
testfs_write_data(struct inode *in, const char *buf, off_t start, size_t size)
{
	char block[BLOCK_SIZE];
	long block_nr = start / BLOCK_SIZE;
	long block_ix = start % BLOCK_SIZE;
	int ret;

        
        
        
	if (block_ix + size > BLOCK_SIZE) {
       
            
         ret = testfs_allocate_block(in, block_nr, block);
       
         if (ret >= 0) {
           
             
             memcpy(block + block_ix, buf, BLOCK_SIZE - block_ix);
        
             write_blocks(in->sb, block, ret, 1);
        
             int index = size - BLOCK_SIZE + block_ix;
            
             int flag = 1;
        
             
        
       
             while(index> BLOCK_SIZE) {
            
                 ret = testfs_allocate_block(in, block_nr + flag, block);
            
               //  if (ret < 0) {
              
                 //    return ret;
           
                 //}
            
                 memcpy(block, buf + BLOCK_SIZE*flag - block_ix, BLOCK_SIZE);
           
                 write_blocks(in->sb, block, ret, 1);
                 
                 flag++;
                 
                 index = index - BLOCK_SIZE;
           
               
            }
        
       
             ret = testfs_allocate_block(in, block_nr + flag, block);
        
       
            // if (ret < 0) {
            
			
            if (ret == -EFBIG) {
                
                     in->in.i_size = 34376597504;
                
                     in->i_flags = I_FLAGS_DIRTY;
            
                 //}
            
                 return  -EFBIG;
        
             }
        
        
             memcpy(block, buf + BLOCK_SIZE*flag - block_ix, index);
        
             write_blocks(in->sb, block, ret, 1);
        
       
             if (size > 0) {
            
                 in->in.i_size = MAX(in->in.i_size, start + (off_t) size);
        
             }
        
             in->i_flags |= I_FLAGS_DIRTY;
        
             return size;
	
            
        
         
    

         }
        
         return ret;
        
        }
	
        
        
        
        
        /* ret is the newly allocated physical block number */
	ret = testfs_allocate_block(in, block_nr, block);
	if (ret < 0)
		return ret;
	memcpy(block + block_ix, buf, size);
	write_blocks(in->sb, block, ret, 1);
	/* increment i_size by the number of bytes written. */
	if (size > 0)
		in->in.i_size = MAX(in->in.i_size, start + (off_t) size);
	in->i_flags |= I_FLAGS_DIRTY;
	/* return the number of bytes written or any error */
	return size;
}

int
testfs_free_blocks(struct inode *in)
{
	int i;
	int e_block_nr;

         char temp_2[BLOCK_SIZE];
                    
         int o;
        
	/* last block number */
	e_block_nr = DIVROUNDUP(in->in.i_size, BLOCK_SIZE);

	/* remove direct blocks */
	for (i = 0; i < e_block_nr && i < NR_DIRECT_BLOCKS; i++) {
		if (in->in.i_block_nr[i] == 0)
			continue;
		testfs_free_block_from_inode(in, in->in.i_block_nr[i]);
		in->in.i_block_nr[i] = 0;
	}
	e_block_nr -= NR_DIRECT_BLOCKS;

	/* remove indirect blocks */
	if (in->in.i_indirect > 0) {
		

		char block[BLOCK_SIZE];
		read_blocks(in->sb, block, in->in.i_indirect, 1);
		for (i = 0; i < e_block_nr && i < NR_INDIRECT_BLOCKS; i++) {
			testfs_free_block_from_inode(in, ((int *)block)[i]);
			((int *)block)[i] = 0;
		}
		testfs_free_block_from_inode(in, in->in.i_indirect);
		in->in.i_indirect = 0;
	}

	


	
	e_block_nr -= NR_INDIRECT_BLOCKS;
	
           
        
      		if (e_block_nr >= 0) {
        
          
            
            
            if (in->in.i_dindirect > 0) {
                
            char temp_block[BLOCK_SIZE];
            
            read_blocks(in->sb, temp_block, in->in.i_dindirect, 1);
           
            int r= 0;
           
            while( e_block_nr > 0 && r < NR_INDIRECT_BLOCKS) {
                
                o = 0;
                
                if (((unsigned int *)temp_block)[r] <= 0) {
                
                     e_block_nr = e_block_nr - NR_INDIRECT_BLOCKS;
                   
                
                }
               
                else {
                
                  
                    
                    read_blocks(in->sb, temp_2, ((unsigned int *)temp_block)[r], 1);
                    
                    
                    
                    while( e_block_nr > 0 && o < NR_INDIRECT_BLOCKS) {
                        
                       
                        
                        testfs_free_block_from_inode(in, ((int *)temp_2)[o]);
                       
                        ((unsigned int *)temp_2)[o] = 0;
                        
                        e_block_nr--;
                        
                        o++;
                    
                    }
                    
                    testfs_free_block_from_inode(in, ((int *)temp_block)[r]);
                    
                    ((unsigned int *)temp_block)[r] = 0;
                
                }
                
                r++ ;
            }
            
            
            testfs_free_block_from_inode(in, in->in.i_dindirect);
           
            in->in.i_dindirect = 0;
       
            
            
            
            
            
            }








	}

	in->in.i_size = 0;
	in->i_flags |= I_FLAGS_DIRTY;
	return 0;
}
